import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:test_thecocktail/domain/entity/cocktail_entity.dart';

class CocktailItemWidget extends StatelessWidget {
  const CocktailItemWidget({
    Key? key,
    required this.cocktail,
  }) : super(key: key);

  final CocktailEntity cocktail;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 10.0,
            // spreadRadius: 10.0,
          ),
        ],
      ),
      child: Container(
        padding: EdgeInsets.all(15.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: CachedNetworkImage(
                imageUrl: cocktail.strDrinkThumb!,
                height: 100.0,
                width: 100.0,
                placeholder: (context, url) => Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: Text(
                cocktail.strDrink!,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
