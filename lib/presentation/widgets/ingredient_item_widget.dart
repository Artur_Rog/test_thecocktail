import 'package:flutter/material.dart';
import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';

class IngredientItemWidget extends StatelessWidget {
  const IngredientItemWidget({
    Key? key,
    required this.onTap,
    required this.ingredient,
  }) : super(key: key);

  final VoidCallback onTap;
  final IngredientEntity ingredient;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 10.0,
            // spreadRadius: 10.0,
          ),
        ],
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(10.0),
          child: Container(
            padding: EdgeInsets.all(15.0),
            child: Text(
              ingredient.strIngredient!,
            ),
          ),
        ),
      ),
    );
  }
}
