import 'package:flutter/material.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({
    Key? key,
    required this.isShowSearchAppbar,
    required this.onChanged,
    required this.focusNode,
    required this.textEditingController,
  }) : super(key: key);

  final bool isShowSearchAppbar;
  final ValueSetter<String> onChanged;
  final TextEditingController textEditingController;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      height: isShowSearchAppbar ? 56.0 : 0.0,
      duration: Duration(milliseconds: 200),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 0.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: TextField(
            controller: textEditingController,
            focusNode: focusNode,
            autofocus: false,
            autocorrect: false,
            style: TextStyle(
              fontSize: 16.0,
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              filled: true,
              contentPadding: EdgeInsets.symmetric(vertical: 13.0, horizontal: 14.0),
              prefixIcon: Icon(
                Icons.search,
              ),
              hintText: 'Ingredient search',
              hintStyle: TextStyle(
                fontSize: 16.0,
              ),
            ),
            onChanged: (String value) {
              onChanged(value);
            },
          ),
        ),
      ),
    );
  }
}
