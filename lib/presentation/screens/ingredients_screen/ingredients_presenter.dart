import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:test_thecocktail/data/repository/remote_cocktails_repository_impl.dart';
import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';
import 'package:test_thecocktail/domain/interactors/get_ingredients_interactor.dart';
import 'package:test_thecocktail/presentation/base/base_presenter.dart';
import 'package:test_thecocktail/presentation/screens/app/app_routes.dart';
import 'package:test_thecocktail/presentation/screens/ingredients_screen/ingredients_model.dart';

class IngredientsPresenter extends BasePresenter<IngredientsModel> {
  IngredientsPresenter(BuildContext context) : super(IngredientsModel(), context);

  GetIngredientsInteractor getIngredientsInteracor =
      GetIngredientsInteractor(RemoteCocktailsRepositoryImpl());

  ScrollController controller = ScrollController();

  TextEditingController textEditingController = TextEditingController();
  FocusNode focusNode = FocusNode();

  @override
  void init() {
    getListIngredients();
    controller.addListener(checkListener);
  }

  void checkListener() {
    if (controller.position.userScrollDirection == ScrollDirection.reverse &&
        model.isShowSearchAppbar == true &&
        textEditingController.text.isEmpty) {
      model.isShowSearchAppbar = false;
      print(controller.position.userScrollDirection);
    }
    if (controller.position.userScrollDirection == ScrollDirection.forward &&
        model.isShowSearchAppbar == false) {
      model.isShowSearchAppbar = true;
      print(controller.position.userScrollDirection);
    }
    updateView();
  }

  Future<void> getListIngredients() async {
    try {
      startLoading();
      model.ingredients = await getIngredientsInteracor.getIngredients();
    } catch (e) {
      print(e);
    } finally {
      endLoading();
    }
  }

  void onIngredientTap(String ingredient) {
    Navigator.pushNamed(
      context,
      AppRoutes.COCKTAILS,
      arguments: ingredient,
    );
    focusNode.unfocus();
  }

  @override
  void dispose() {
    controller.removeListener(checkListener);
    controller.dispose();
    super.dispose();
  }

  void setSearchValue(String value) {
    model.ingredientsSubList.clear();
    model.ingredients.forEach((IngredientEntity ingredientEntity) {
      if (ingredientEntity.strIngredient!.toLowerCase().contains(value.toLowerCase())) {
        model.ingredientsSubList.add(ingredientEntity);
      }
    });
    updateView();
  }
}
