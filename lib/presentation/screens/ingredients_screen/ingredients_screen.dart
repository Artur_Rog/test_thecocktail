import 'package:flutter/material.dart';
import 'package:test_thecocktail/generated/l10n.dart';
import 'package:test_thecocktail/presentation/screens/ingredients_screen/ingredients_model.dart';
import 'package:test_thecocktail/presentation/screens/ingredients_screen/ingredients_presenter.dart';
import 'package:test_thecocktail/presentation/widgets/ingredient_item_widget.dart';
import 'package:test_thecocktail/presentation/widgets/search_widget.dart';

class IngredientsScreen extends StatefulWidget {
  const IngredientsScreen({Key? key}) : super(key: key);

  @override
  _IngredientsScreenState createState() => _IngredientsScreenState();
}

class _IngredientsScreenState extends State<IngredientsScreen> {
  IngredientsPresenter? _presenter;

  @override
  void didChangeDependencies() {
    if (_presenter == null) {
      _presenter = IngredientsPresenter(context);
      _presenter!.init();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).cocktailIngredientsTitle),
      ),
      body: StreamBuilder<IngredientsModel>(
          initialData: _presenter?.model,
          stream: _presenter?.stream,
          builder: (context, snapshot) {
            IngredientsModel? ingredientsModel = snapshot.data;
            if (ingredientsModel!.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            return Column(
              children: [
                SearchWidget(
                  isShowSearchAppbar: ingredientsModel.isShowSearchAppbar,
                  focusNode: _presenter!.focusNode,
                  onChanged: _presenter!.setSearchValue,
                  textEditingController: _presenter!.textEditingController,
                ),
                if (ingredientsModel.ingredientsSubList.isNotEmpty ||
                    _presenter!.textEditingController.text.isNotEmpty)
                  Expanded(
                    child: ListView.separated(
                      controller: _presenter?.controller,
                      padding: EdgeInsets.all(15.0),
                      itemCount: ingredientsModel.ingredientsSubList.length,
                      itemBuilder: (context, index) {
                        return IngredientItemWidget(
                          onTap: () => _presenter?.onIngredientTap(
                              ingredientsModel.ingredientsSubList[index].strIngredient!),
                          ingredient: ingredientsModel.ingredientsSubList[index],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 10.0,
                        );
                      },
                    ),
                  )
                else
                  Expanded(
                    child: ListView.separated(
                      controller: _presenter?.controller,
                      padding: EdgeInsets.all(15.0),
                      itemCount: ingredientsModel.ingredients.length,
                      itemBuilder: (context, index) {
                        return IngredientItemWidget(
                          onTap: () => _presenter
                              ?.onIngredientTap(ingredientsModel.ingredients[index].strIngredient!),
                          ingredient: ingredientsModel.ingredients[index],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 10.0,
                        );
                      },
                    ),
                  ),
              ],
            );
          }),
    );
  }
}
