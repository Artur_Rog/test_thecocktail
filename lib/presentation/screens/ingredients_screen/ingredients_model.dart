import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';
import 'package:test_thecocktail/presentation/base/base_screen_view_model.dart';

class IngredientsModel extends BaseScreenViewModel {
  List<IngredientEntity> ingredients = [];
  List<IngredientEntity> ingredientsSubList = [];
  bool isShowSearchAppbar = true;
}
