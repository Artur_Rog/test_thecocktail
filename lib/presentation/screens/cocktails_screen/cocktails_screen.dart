import 'package:flutter/material.dart';
import 'package:test_thecocktail/generated/l10n.dart';
import 'package:test_thecocktail/presentation/screens/cocktails_screen/cocktails_model.dart';
import 'package:test_thecocktail/presentation/screens/cocktails_screen/cocktails_presenter.dart';
import 'package:test_thecocktail/presentation/widgets/cocktail_item_widget.dart';

class CocktailsScreen extends StatefulWidget {
  const CocktailsScreen({Key? key}) : super(key: key);

  @override
  _CocktailsScreenState createState() => _CocktailsScreenState();
}

class _CocktailsScreenState extends State<CocktailsScreen> {
  CocktailsPresenter? _presenter;

  @override
  void didChangeDependencies() {
    if (_presenter == null) {
      _presenter = CocktailsPresenter(context);
      _presenter!.init();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder<CocktailsModel>(
            initialData: _presenter?.model,
            stream: _presenter?.stream,
            builder: (context, snapshot) {
              CocktailsModel? cocktailsModel = snapshot.data;
              return Text(S.of(context).cocktailsScreenTitle(cocktailsModel!.ingredient!));
            }),
      ),
      body: StreamBuilder<CocktailsModel>(
          initialData: _presenter?.model,
          stream: _presenter?.stream,
          builder: (context, snapshot) {
            CocktailsModel? cocktailsModel = snapshot.data;
            if (cocktailsModel!.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            return ListView.separated(
              padding: EdgeInsets.all(15.0),
              itemCount: cocktailsModel.cocktails.length,
              itemBuilder: (context, index) {
                return CocktailItemWidget(cocktail: cocktailsModel.cocktails[index]);
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 10.0,
                );
              },
            );
          }),
    );
  }
}
