import 'package:test_thecocktail/domain/entity/cocktail_entity.dart';
import 'package:test_thecocktail/presentation/base/base_screen_view_model.dart';

class CocktailsModel extends BaseScreenViewModel {
  String? ingredient;
  List<CocktailEntity> cocktails = [];
}
