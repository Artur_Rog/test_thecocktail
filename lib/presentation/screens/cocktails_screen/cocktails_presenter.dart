import 'package:flutter/material.dart';
import 'package:test_thecocktail/data/repository/remote_cocktails_repository_impl.dart';
import 'package:test_thecocktail/domain/interactors/get_cocktails_by_ingredient_interactor.dart';
import 'package:test_thecocktail/presentation/base/base_presenter.dart';

import 'cocktails_model.dart';

class CocktailsPresenter extends BasePresenter<CocktailsModel> {
  CocktailsPresenter(BuildContext context) : super(CocktailsModel(), context);

  GetCocktailsByIngredientsInteractor getCocktailsByIngredientsInteractor =
      GetCocktailsByIngredientsInteractor(RemoteCocktailsRepositoryImpl());

  @override
  void init() {
    var arg = ModalRoute.of(context)?.settings.arguments;
    if (arg != null && arg is String) {
      model.ingredient = arg;
    }
    getListIngredients();
  }

  Future<void> getListIngredients() async {
    try {
      startLoading();
      model.cocktails = await getCocktailsByIngredientsInteractor.getIngredients(model.ingredient!);
    } catch (e) {
      print(e);
    } finally {
      endLoading();
    }
  }
}
