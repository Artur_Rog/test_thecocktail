class AppRoutes {
  static const String INGREDIENTS = '/ingredients';
  static const String COCKTAILS = '/cocktails';
}
