import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:test_thecocktail/generated/l10n.dart';
import 'package:test_thecocktail/presentation/screens/cocktails_screen/cocktails_screen.dart';
import 'package:test_thecocktail/presentation/screens/ingredients_screen/ingredients_screen.dart';

import 'app_routes.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cocktails Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: AppRoutes.INGREDIENTS,
      routes: {
        AppRoutes.INGREDIENTS: (context) => IngredientsScreen(),
        AppRoutes.COCKTAILS: (context) => CocktailsScreen(),
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        S.delegate,
      ],
    );
  }
}
