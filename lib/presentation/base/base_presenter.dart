import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'app_state_enum.dart';
import 'base_screen_view_model.dart';

abstract class BasePresenter<M extends BaseScreenViewModel> {
  M model;
  BehaviorSubject<M> _subject = BehaviorSubject<M>();
  BuildContext context;

  Stream<M> get stream => _subject.stream;

  // ExceptionHelper exceptionHelper = ExceptionHelper();

  BasePresenter(this.model, this.context) {
    init();
  }

  void init();

  void dispose() {
    _subject.close();
  }

  void updateView() {
    if (!_subject.isClosed) _subject.sink.add(model);
  }

  void startLoading() {
    model.state = ScreenState.Loading;
    updateView();
  }

  void startUpdating() {
    model.state = ScreenState.Updating;
    updateView();
  }

  void endLoading() {
    model.state = ScreenState.Done;
    updateView();
  }

  void startReading() {
    model.state = ScreenState.Reading;
    updateView();
  }

  void endLoadingWithError() {
    model.state = ScreenState.Error;
    updateView();
  }
}
