import 'app_state_enum.dart';

class BaseScreenViewModel {
  ScreenState state = ScreenState.Loading;

  bool get isLoading => state.isLoading;

  bool get isUpdating => state.isUpdating;

  bool get isDone => state.isDone;

  bool get isError => state.isError;

  bool get isReading => state.isReading;

  bool get isUploading => state.isUploading;

  bool get isNone => state.isNone;
}
