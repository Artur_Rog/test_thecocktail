class CocktailEntity {
  CocktailEntity({
    this.strDrink,
    this.strDrinkThumb,
    this.idDrink,
  });

  String? strDrink;
  String? strDrinkThumb;
  String? idDrink;
}
