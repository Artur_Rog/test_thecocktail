import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';
import 'package:test_thecocktail/domain/repository/cocktails_repository.dart';

class GetIngredientsInteractor {
  CocktailsRepository _repository;

  GetIngredientsInteractor(this._repository);

  Future<List<IngredientEntity>> getIngredients() async {
    return _repository.getIngredients();
  }
}
