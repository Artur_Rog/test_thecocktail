import 'package:test_thecocktail/domain/entity/cocktail_entity.dart';
import 'package:test_thecocktail/domain/repository/cocktails_repository.dart';

class GetCocktailsByIngredientsInteractor {
  CocktailsRepository _repository;

  GetCocktailsByIngredientsInteractor(this._repository);

  Future<List<CocktailEntity>> getIngredients(String ingredient) async {
    return _repository.getCocktailsByIngredient(ingredient);
  }
}
