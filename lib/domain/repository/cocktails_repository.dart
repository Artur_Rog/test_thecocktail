import 'package:test_thecocktail/domain/entity/cocktail_entity.dart';
import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';

abstract class CocktailsRepository {
  Future<List<IngredientEntity>> getIngredients();

  Future<List<CocktailEntity>> getCocktailsByIngredient(String ingredient);
}
