import 'package:test_thecocktail/data/entry/cocktail_entry.dart';
import 'package:test_thecocktail/data/entry/ingredients_entry.dart';
import 'package:test_thecocktail/domain/entity/cocktail_entity.dart';
import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';

class CocktailsMapper {
  static List<IngredientEntity> mapToIngredientEntity(IngredientsEntry entry) {
    return entry.drinks!.map((e) => IngredientEntity(e.strIngredient1)).toList();
  }

  static List<CocktailEntity> mapToCocktailEntity(CocktailsEntry entry) {
    return entry.drinks!
        .map((e) => CocktailEntity(
              idDrink: e.idDrink,
              strDrink: e.strDrink,
              strDrinkThumb: e.strDrinkThumb,
            ))
        .toList();
  }
}
