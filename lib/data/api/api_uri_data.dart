class ApiUriData {
  static const String schema = 'https';
  static const String list = 'list.php';
  static const String filter = 'filter.php';
  static const String path = '/api/json/v1/$apiKey/';

  static const apiKey = 1;
  static const baseUrl = 'www.thecocktaildb.com';

  static Uri getListIngredientsUri() {
    var ingredientParams = {'i': 'list'};
    return Uri.https(baseUrl, path + list, ingredientParams);
  }

  static Uri getListCocktailsByIngredientsUri(String ingredient) {
    var ingredientParams = {'i': ingredient};
    return Uri.https(baseUrl, path + filter, ingredientParams);
  }
}
