import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_thecocktail/data/api/api_uri_data.dart';
import 'package:test_thecocktail/data/entry/cocktail_entry.dart';
import 'package:test_thecocktail/data/entry/ingredients_entry.dart';

class ApiClient {
  Future<IngredientsEntry> getIngredients() async {
    http.Response response = await http.get(ApiUriData.getListIngredientsUri());
    return IngredientsEntry.fromJson(jsonDecode(response.body));
  }

  Future<CocktailsEntry> getCocktailsByIngredient(String ingredient) async {
    http.Response response =
        await http.get(ApiUriData.getListCocktailsByIngredientsUri(ingredient));
    return CocktailsEntry.fromJson(jsonDecode(response.body));
  }
}
