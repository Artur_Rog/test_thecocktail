import 'package:test_thecocktail/data/api/api_client.dart';
import 'package:test_thecocktail/data/entry/cocktail_entry.dart';
import 'package:test_thecocktail/data/entry/ingredients_entry.dart';
import 'package:test_thecocktail/data/mappers/cocktails_mapper.dart';
import 'package:test_thecocktail/domain/entity/cocktail_entity.dart';
import 'package:test_thecocktail/domain/entity/ingredients_entity.dart';
import 'package:test_thecocktail/domain/repository/cocktails_repository.dart';

class RemoteCocktailsRepositoryImpl extends CocktailsRepository {
  ApiClient _apiClient = ApiClient();

  @override
  Future<List<IngredientEntity>> getIngredients() async {
    IngredientsEntry entry = await _apiClient.getIngredients();
    return CocktailsMapper.mapToIngredientEntity(entry);
  }

  @override
  Future<List<CocktailEntity>> getCocktailsByIngredient(String ingredient) async {
    CocktailsEntry entry = await _apiClient.getCocktailsByIngredient(ingredient);
    return CocktailsMapper.mapToCocktailEntity(entry);
  }
}
