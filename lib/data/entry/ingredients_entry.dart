// To parse this JSON data, do
//
//     final ingredientsEntity = ingredientsEntityFromJson(jsonString);

import 'dart:convert';

IngredientsEntry ingredientsEntityFromJson(String str) =>
    IngredientsEntry.fromJson(json.decode(str));

String ingredientsEntityToJson(IngredientsEntry data) => json.encode(data.toJson());

class IngredientsEntry {
  IngredientsEntry({
    this.drinks,
  });

  List<Drink>? drinks;

  IngredientsEntry copyWith({
    List<Drink>? drinks,
  }) =>
      IngredientsEntry(
        drinks: drinks ?? this.drinks,
      );

  factory IngredientsEntry.fromJson(Map<String, dynamic> json) => IngredientsEntry(
        drinks: json["drinks"] == null
            ? null
            : List<Drink>.from(json["drinks"].map((x) => Drink.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "drinks": drinks == null ? null : List<dynamic>.from(drinks!.map((x) => x.toJson())),
      };
}

class Drink {
  Drink({
    this.strIngredient1,
  });

  String? strIngredient1;

  Drink copyWith({
    String? strIngredient1,
  }) =>
      Drink(
        strIngredient1: strIngredient1 ?? this.strIngredient1,
      );

  factory Drink.fromJson(Map<String, dynamic> json) => Drink(
        strIngredient1: json["strIngredient1"] == null ? null : json["strIngredient1"],
      );

  Map<String, dynamic> toJson() => {
        "strIngredient1": strIngredient1 == null ? null : strIngredient1,
      };
}
