import 'dart:convert';

CocktailsEntry cocktailFromJson(String str) => CocktailsEntry.fromJson(json.decode(str));

String cocktailToJson(CocktailsEntry data) => json.encode(data.toJson());

class CocktailsEntry {
  CocktailsEntry({
    this.drinks,
  });

  List<Drink>? drinks;

  CocktailsEntry copyWith({
    List<Drink>? drinks,
  }) =>
      CocktailsEntry(
        drinks: drinks ?? this.drinks,
      );

  factory CocktailsEntry.fromJson(Map<String, dynamic> json) => CocktailsEntry(
        drinks: json["drinks"] == null
            ? null
            : List<Drink>.from(json["drinks"].map((x) => Drink.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "drinks": drinks == null ? null : List<dynamic>.from(drinks!.map((x) => x.toJson())),
      };
}

class Drink {
  Drink({
    this.strDrink,
    this.strDrinkThumb,
    this.idDrink,
  });

  String? strDrink;
  String? strDrinkThumb;
  String? idDrink;

  Drink copyWith({
    String? strDrink,
    String? strDrinkThumb,
    String? idDrink,
  }) =>
      Drink(
        strDrink: strDrink ?? this.strDrink,
        strDrinkThumb: strDrinkThumb ?? this.strDrinkThumb,
        idDrink: idDrink ?? this.idDrink,
      );

  factory Drink.fromJson(Map<String, dynamic> json) => Drink(
        strDrink: json["strDrink"] == null ? null : json["strDrink"],
        strDrinkThumb: json["strDrinkThumb"] == null ? null : json["strDrinkThumb"],
        idDrink: json["idDrink"] == null ? null : json["idDrink"],
      );

  Map<String, dynamic> toJson() => {
        "strDrink": strDrink == null ? null : strDrink,
        "strDrinkThumb": strDrinkThumb == null ? null : strDrinkThumb,
        "idDrink": idDrink == null ? null : idDrink,
      };
}
